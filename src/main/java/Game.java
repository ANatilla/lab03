//Game class implementation
public class Game
{
  private int score;            //stores the score of the current Game
  private int t1_modifier;      //stores how many times the scored points have to be added in the next turn
  private int t2_modifier;      //stores how many times the scored points have to be added in the turn after the next one
  private int half_turn_score;  //stores how many points have been scored in the first half of the frame
  private int turn_counter;     //stores how many turns have been played

  Game()
  {
    this.score = 0;
    this.t1_modifier = 0;
    this.t2_modifier = 0;
    this.half_turn_score = -1;
    this.turn_counter = 0;
  }

  //Returns the score of the Game in the moment it is called
  public int score()
  {
    return this.score;
  }

  //
  public void roll(int n)
  {
    this.score += n * (this.t1_modifier + 1);
    this.t1_modifier = this.t2_modifier;
    this.t2_modifier = 0;

    //It means there's been no other shot in the frame
    //and this is the first shot of the frame; it is a Strike since n == 10.
    if(this.half_turn_score == -1 && n == 10)
    {
      if(this.turn_counter < 9) //if we're before the 10th frame
      {
        this.t1_modifier++;
        this.t2_modifier++;
      }
      this.turn_counter++; //advances turn
    }
    else if(this.half_turn_score + n == 10) //that is a Spare
    {
      if(this.turn_counter < 9) this.t1_modifier++;
      this.half_turn_score = -1; //"resets" the frame
      this.turn_counter++;
    }
    else if(this.half_turn_score == -1) //generic first shot of the frame
    {
      this.half_turn_score = n;
    }
    else //generic second shot of the frame
    {
      this.half_turn_score = -1;
      this.turn_counter++;
    }
  }
}
